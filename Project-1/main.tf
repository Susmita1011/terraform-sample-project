provider "aws"{
    region = "ca-central-1"
     access_key = "AKIAXWAB6D3EK2N4GG4M"
     secret_key = "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"
}

#1.create vpc

resource "aws_vpc" "my-prod-vpc" {
      cidr_block = "10.0.0.0/16"

      tags = {
          Name = "production"
      }
  
}

#2.Create Internet Gateway

resource "aws_internet_gateway" "gw-1" {
  vpc_id = aws_vpc.my-prod-vpc.id

  tags = {
    Name = "production-igw"
  }
}

#3.Create custom Route table:

resource "aws_route_table" "rt-1" {
  vpc_id = aws_vpc.my-prod-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw-1.id
  }
  route {
  ipv6_cidr_block = "::/0"
  gateway_id = aws_internet_gateway.gw-1.id
  }
  tags = {
    Name = "production-route"
  }
}

#4.Create a Subnet

resource "aws_subnet" "my-prod-subnet" {
  vpc_id     = aws_vpc.my-prod-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ca-central-1a"

  tags = {
    Name = "production-subnet-1"
  }
}

#5.Associate subnet with Route Table

resource "aws_route_table_association" "rt-a-sub-1" {
  subnet_id      = aws_subnet.my-prod-subnet.id
  route_table_id = aws_route_table.rt-1.id
}

#6. Create security group to allow port 22, 80,443

resource "aws_security_group" "prod-security-group" {
  name        = "production-security-group"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.my-prod-vpc.id

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
   ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
   ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
   egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
   }

  tags = {
    Name = "prod_security_group"
  }
}

#7. Create a network interface with an ip in the subnet that was created in step 4:

resource "aws_network_interface" "prod-eni" {
  subnet_id       = aws_subnet.my-prod-subnet.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.prod-security-group.id]

}



#8.Assign an eip to nw interface in 7:

resource "aws_eip" "prod-eip" {
vpc                       = true
  network_interface         = aws_network_interface.prod-eni.id
  associate_with_private_ip = "10.0.1.50"
  depends_on                = [aws_internet_gateway.gw-1]
}
  

output "server_public_ip"{
value = aws_eip.prod-eip.public_ip
}


#9. Create Ubuntu server and install /enable apache2

resource "aws_instance" "prod-web-server"{
    availability_zone = "ca-central-1a"
    ami = "ami-0a7154091c5c6623e"
    instance_type = "t2.micro"
    key_name = "web_server_ca"
	
    network_interface {
	device_index         = 0
    network_interface_id = aws_network_interface.prod-eni.id
    
  }
user_data = file("./script.sh")
         
tags = {
     Name = "Web-server-Demo"
}
}