terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}
provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "test" {
  name = "DemoRG"
}
#creating resource group

#resource "azurerm_resource_group" "dev-resource-grp" {
 # name     = "dev-resource-grp"
  #location = "Central India"
  #tags = {
   # envionment = "dev"
  #}
#}

resource "azurerm_virtual_network" "dev-vnet" {
  name                = "dev-vnet"
  location            ="${data.azurerm_resource_group.test.location}"
  resource_group_name = "${data.azurerm_resource_group.test.name}"
  address_space       = ["10.0.0.0/16"]

  tags = {
    envionment = "dev"
  }
}
resource "azurerm_subnet" "dev-subnet" {
  name                 = "dev-subnet"
  resource_group_name  = "${data.azurerm_resource_group.test.name}"
  virtual_network_name = azurerm_virtual_network.dev-vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}


resource "azurerm_network_security_group" "dev-security-grp" {
  name                = "dev-security-grp"
  location            = "${data.azurerm_resource_group.test.location}"
  resource_group_name = "${data.azurerm_resource_group.test.name}"

  tags = {
    envionment = "dev"
  }
}


#creating network security rule

resource "azurerm_network_security_rule" "dev-security-rule" {
  name                        = "dev-security-rule"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${data.azurerm_resource_group.test.name}"
  network_security_group_name = azurerm_network_security_group.dev-security-grp.name
}

#creating network,subnet,security group association:

resource "azurerm_subnet_network_security_group_association" "dev-nssg" {
  subnet_id                 = azurerm_subnet.dev-subnet.id
  network_security_group_id = azurerm_network_security_group.dev-security-grp.id
}

resource "azurerm_public_ip" "dev-pub-ip" {
  name                = "dev-pub-ip"
  resource_group_name = "${data.azurerm_resource_group.test.name}"
  location            = "${data.azurerm_resource_group.test.location}"
  allocation_method   = "Dynamic"

  tags = {
    environment = "dev"
  }
}



#creating the nic for the azure vm:

resource "azurerm_network_interface" "dev-nic" {
  name                = "dev-nic"
  location            = "${data.azurerm_resource_group.test.location}"
  resource_group_name = "${data.azurerm_resource_group.test.name}"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.dev-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.dev-pub-ip.id
  }
  tags = {
    environment = "dev"
  }

}
#creating linux vm:

resource "azurerm_linux_virtual_machine" "dev-webserver" {
  name                  = "dev-webserver"
  resource_group_name   = "${data.azurerm_resource_group.test.name}"
  location              = "${data.azurerm_resource_group.test.location}"
  size                  = "Standard_B1s"
  admin_username        = "adminuser"
  network_interface_ids = [azurerm_network_interface.dev-nic.id]

  custom_data = filebase64("customdata.tpl")

  admin_ssh_key {
   username   = "adminuser"
    public_key = file("~/.ssh/key.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  tags = {
    environment = "dev"
  }
}













